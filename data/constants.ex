defmodule Constants do
    def country_codes do

        [
            "VE", "EE", "QA", "KH", "PE", "TM", "AM", "VC"
        ]
    end
    def fruits do
        
        [
            "Açaí","Akee","Apple","Apricot","Avocado","Banana",
            "Bilberry", "Blackberry", "Blackcurrant","Blueberry",
            "Boysenberry","Crab apples","Currant","Cherry", "Cherimoya"
           
        ]
        
    end
end
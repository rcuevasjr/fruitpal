defmodule GenJson do


    def main do

        data =
        Enum.map(Constants.country_codes, fn cc -> build_payload(cc) end)
        |> List.flatten
        |> Poison.encode!

         File.write!("./data/data.json", data)


    end

    def build_payload(country_code) do


        Constants.fruits
        |> Enum.map(fn fruit ->

            %{
                "COUNTRY": country_code,
                "COMMODITY": fruit,
                "FIXED_OVERHEAD": FakerElixir.Number.between(24.37, 100.25) |> inspect,
                "VARIABLE_OVERHEAD": FakerElixir.Number.between(1.67, 3.19) |> inspect
            }

        end)

    end
  end


  GenJson.main()

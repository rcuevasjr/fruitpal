
defmodule Fruitpal do

  #Entrypoint. Returns array of structured strings
  def main(%{commodity: commodity, ppt: ppt, volume: volume}) do

     data = File.read!("./data/data.json") |> Poison.decode!

     commodity = commodity |> String.downcase

     data
     |> Stream.filter(fn record -> record["COMMODITY"] |> String.downcase == commodity end)
     |> Enum.map(fn record -> build_string(record,ppt,volume)  end)


  end


  #Builds strings per each returned record
  def build_string(obj,ppt,volume) do


      country_code = obj["COUNTRY"]
      {fixed_oh, _} = obj["FIXED_OVERHEAD"] |> Float.parse
      {var_oh, _} = obj["VARIABLE_OVERHEAD"] |> Float.parse
      total = calculate_total(ppt, volume, fixed_oh, var_oh) |> Float.round(2)

    "< #{country_code} #{total} | (#{ppt + var_oh} * #{volume}) + #{fixed_oh}"

  end

  # Calculates total price
  def calculate_total(ppt, volume, fixed_oh, var_oh) do
    total_ppt = ppt + var_oh
    (total_ppt * volume) + fixed_oh

  end


end

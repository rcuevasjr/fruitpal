defmodule CLI do


    #Entry Point for Application. Takes Arguments, validates, and  used Fruitpal
    def main(args) do
      # args |> IO.inspect
      options = [switches: [commodity: :stirng , ppt: :float, volume: :float], aliases: [ c: :commodity, p: :ppt, v: :volume]]
      {params,_,_}= OptionParser.parse(args, options)

      payload = params |> Enum.into(%{})
      payload |> validate

      response = payload |>  Fruitpal.main
      case response |> length do
          0 ->
              return = "No results found"
              return |> IO.puts
              return
          _ ->
              response |> Enum.each(fn x ->  x |> IO.puts end)
              return = response
      end
    end


    #Validates if all the correct flags were passed to cli
    def validate(%{commodity: commodity, ppt: ppt, volume: volume}) do
        true
    end
    def validate(_) do
        raise ArgumentError, message: "please specify the following flags: --commodity --ppt(price per ton), --volume(in tons)"

    end

  end

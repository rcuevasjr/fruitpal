# Fruitpal


Dependencies:
Docker
----------------------------------------------

Interacting with the container:
1) sh init.sh

---------------------------------------------------

Using CLI Tool
Steps:
1) EXAMPLE:  ./fruitpal --commodity=Apple --ppt=100 --volume=50

Options of commodities:

[
    "Açaí","Akee","Apple","Apricot","Avocado","Banana",
    "Bilberry", "Blackberry", "Blackcurrant","Blueberry",
    "Boysenberry","Crab apples","Currant","Cherry", "Cherimoya"

]

-----------------------------------------

Tests
Steps:
1) mix test

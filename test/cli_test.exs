defmodule CLITest do
  use ExUnit.Case


  test "should return custom message if no results found" do
      args = ["--commodity=Mamey", "--ppt=100", "--volume=49"]
      assert CLI.main(args) == "No results found"
  end


  test "should return results from valid payload" do
      args = ["--commodity=Apple", "--ppt=100", "--volume=49"]
      assert CLI.main(args) |> length  > 0
  end

  test "validates cli arguments" do


    assert CLI.validate(%{commodity: "fruit", ppt: 50, volume: 30}) == true

  end

  test "In validates cli arguments" do

      assert_raise ArgumentError,
      "please specify the following flags: --commodity --ppt(price per ton), --volume(in tons)",
       fn -> CLI.validate(%{commodity: "fruit"}) end

  end


end

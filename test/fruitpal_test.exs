defmodule FruitpalTest do
  use ExUnit.Case


  test "calculates total correctly" do

    obj = %{"VARIABLE_OVERHEAD" => "1.82","FIXED_OVERHEAD" => "59.26","COUNTRY" => "VE","COMMODITY" => "Açaí"}
    fixed_oh = obj["FIXED_OVERHEAD"]
    var_oh = obj["VARIABLE_OVERHEAD"]
    {fixed_oh, _} = obj["FIXED_OVERHEAD"] |> Float.parse
    {var_oh, _} = obj["VARIABLE_OVERHEAD"] |> Float.parse
    ppt = 1000
    volume = 250
    total_ppt = ppt + var_oh
    total =  (total_ppt * volume) + fixed_oh

    assert Fruitpal.calculate_total(ppt, volume, fixed_oh, var_oh) == total

  end



  test "builds string correctly" do

    obj = %{"VARIABLE_OVERHEAD" => "1.82","FIXED_OVERHEAD" => "59.26","COUNTRY" => "VE","COMMODITY" => "Açaí"}
    country_code = obj["COUNTRY"]
    fixed_oh = obj["FIXED_OVERHEAD"]
    var_oh = obj["VARIABLE_OVERHEAD"]
    {fixed_oh, _} = obj["FIXED_OVERHEAD"] |> Float.parse
    {var_oh, _} = obj["VARIABLE_OVERHEAD"] |> Float.parse
    ppt = 1000
    volume = 250
    total =  Fruitpal.calculate_total(ppt, volume, fixed_oh, var_oh) |> Float.round(2)


    assert Fruitpal.build_string(obj,ppt,volume) == "< #{country_code} #{total} | (#{ppt + var_oh} * #{volume}) + #{fixed_oh}"


  end



end
